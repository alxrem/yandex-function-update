BINARY := yandex-function-update
SOURCES := $(wildcard *.go)

default: test $(BINARY)

$(BINARY): $(SOURCES)
	CGO_ENABLED=0 go build -o $(BINARY)

test: $(SOURCES)
	go test ./...

clean:
	rm -f $(BINARY)
	rm -rf dist/

release:
	goreleaser release --rm-dist

snapshot:
	goreleaser release --rm-dist --snapshot

.PHONY: test clean release
