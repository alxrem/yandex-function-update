// Copyright 2021 Alexey Remizov <alexey@remizov.org>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"context"
	"flag"
	"github.com/yandex-cloud/go-genproto/yandex/cloud/serverless/functions/v1"
	"github.com/yandex-cloud/go-sdk"
	"github.com/yandex-cloud/go-sdk/iamkey"
	"log"
	"os"
	"strings"
)

func main() {
	token := os.Getenv("YANDEX_TOKEN")
	serviceAccountKey := os.Getenv("YANDEX_SERVICE_ACCOUNT_KEY")
	functionId := os.Getenv("YANDEX_FUNCTION_ID")
	sourceTag := os.Getenv("YANDEX_FUNCTION_SOURCE_TAG")
	packageBucket := os.Getenv("YANDEX_FUNCTION_PACKAGE_BUCKET")
	packageObject := os.Getenv("YANDEX_FUNCTION_PACKAGE_OBJECT")
	contentFile := os.Getenv("YANDEX_FUNCTION_CONTENT")

	failIfNoFunction := flag.Bool("fail-if-no-function", false, "Fail execution if function is not exists")
	flag.Parse()

	if sourceTag == "" {
		sourceTag = "$latest"
	}

	req := &functions.CreateFunctionVersionRequest{}
	if contentFile != "" {
		content, err := os.ReadFile(contentFile)
		if err != nil {
			log.Fatalf("Failed to read content: %s", err)
		}

		req.PackageSource = &functions.CreateFunctionVersionRequest_Content{
			Content: content,
		}
	} else if packageBucket != "" && packageObject != "" {
		req.PackageSource = &functions.CreateFunctionVersionRequest_Package{
			Package: &functions.Package{
				BucketName: packageBucket,
				ObjectName: packageObject,
			},
		}
	} else {
		log.Fatalf("YANDEX_FUNCTION_CONTENT or YANDEX_FUNCTION_PACKAGE_* is required")
	}

	if functionId == "" {
		log.Fatalf("YANDEX_FUNCTION_ID is required")
	}

	ctx := context.Background()
	config := ycsdk.Config{}
	if token != "" {
		config.Credentials = ycsdk.OAuthToken(token)
	} else if serviceAccountKey != "" {
		credentials, err := loadServiceAccountKey(serviceAccountKey)
		if err != nil {
			log.Fatalf("Failed to load service account key: %s", err)
		}
		config.Credentials = credentials
	} else {
		log.Fatalf("YANDEX_TOKEN or YANDEX_SERVICE_ACCOUNT_KEY is required")
	}

	sdk, err := ycsdk.Build(ctx, config)
	if err != nil {
		log.Fatalf("Failed to build SDK: %s", err)
	}

	f, err := sdk.Serverless().Functions().Function().Get(ctx, &functions.GetFunctionRequest{
		FunctionId: functionId,
	})
	if err != nil {
		if !*failIfNoFunction && strings.Contains(err.Error(), "NotFound") {
			return
		}
		log.Fatalf("Failed to get function: %s", err)
	}

	v, err := sdk.Serverless().Functions().Function().GetVersionByTag(ctx, &functions.GetFunctionVersionByTagRequest{FunctionId: f.GetId(), Tag: sourceTag})
	if err != nil {
		log.Fatalf("Failed to get version: %s", err)
	}

	req.FunctionId = v.FunctionId
	req.Runtime = v.Runtime
	req.Description = v.Description
	req.Entrypoint = v.Entrypoint
	req.Resources = v.Resources
	req.ExecutionTimeout = v.ExecutionTimeout
	req.ServiceAccountId = v.ServiceAccountId
	req.Environment = v.Environment
	req.Connectivity = v.Connectivity
	req.NamedServiceAccounts = v.NamedServiceAccounts

	if _, err := sdk.Serverless().Functions().Function().CreateVersion(ctx, req); err != nil {
		log.Fatalf("Failed to create version: %s", err)
	}
}

func loadServiceAccountKey(keyFile string) (ycsdk.Credentials, error) {
	key, err := iamkey.ReadFromJSONFile(keyFile)
	if err != nil {
		return nil, err
	}
	credentials, err := ycsdk.ServiceAccountKey(key)
	if err != nil {
		return nil, err
	}
	return credentials, nil
}
