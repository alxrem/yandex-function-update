module gitlab.com/alxrem/yandex-function-update

go 1.16

require (
	github.com/yandex-cloud/go-genproto v0.0.0-20211115083454-9ca41db5ed9e
	github.com/yandex-cloud/go-sdk v0.0.0-20211116090820-88eace59470c
)
